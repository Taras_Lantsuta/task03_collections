package com.epam;

public class DroidsShip<T1,T2> {
    private T1 name;
    private T2 droid;

    public DroidsShip(T1 name,T2 droid) {
        this.name = name;
        this.droid = droid;
    }

    public T1 getName(){
        return name;
    }
    public T2 getDroid() {
        return droid;
    }

}
