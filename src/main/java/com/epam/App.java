package com.epam;

public class App {
    public static void main(String[] args) {
        DroidsShip<Long,Droid> ship = new DroidsShip<>(1l,new Droid());
        System.out.print(ship.getName()+ " ");
        System.out.println(ship.getDroid());
        MyDeque<String> myDeque = new MyDeque<>();
        myDeque.addFirst("A");
        myDeque.addFirst("B");
        myDeque.addFirst("C");
        myDeque.addFirst("D");
        myDeque.addFirst("E");
        myDeque.addFirst("C");
        myDeque.addFirst("F");
        System.out.println(myDeque);
        myDeque.removeFirstOccurrence("C");
        System.out.println(myDeque);
    }

    public static class Droid {
        @Override
        public String toString() {
            return "R2D2";
        }
    }
}
