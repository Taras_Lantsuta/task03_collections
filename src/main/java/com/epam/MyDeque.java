package com.epam;

import java.util.*;

public class MyDeque<E> implements Deque<E> {
    private List<E> list = new ArrayList<>();

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

    @Override
    public Iterator<E> descendingIterator() {
        LinkedList<E> linkedList = (LinkedList<E>) list;
        return linkedList.descendingIterator();
    }

    @Override
    public void addFirst(E e) {
        if (list.isEmpty()) {
            list.add(e);
            return;
        }
        list.size();
        int i = list.size() - 1;
        list.add(0, e);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public void addLast(E e) {
        list.add(e);
    }

    @Override
    public boolean offerFirst(E e) {
        if (e != null) {
            addFirst(e);
            return true;
        }
        return false;
    }


    @Override
    public boolean offerLast(E e) {
        if (e != null) {
            list.add(e);
            return true;
        }
        return false;
    }

    @Override
    public E removeFirst() {
        E element = list.get(0);
        list.remove(0);
        return element;
    }

    @Override
    public E removeLast() {
        E element = list.get(list.size() - 1);
        list.remove(list.size() - 1);
        return element;
    }

    @Override
    public E pollFirst() {
        E element;
        if (list.get(0) == null) {
            return null;
        }
        element = list.get(0);
        removeFirst();
        return element;
    }

    @Override
    public E pollLast() {
        E element;
        if (list.get(list.size() - 1) == null) {
            return null;
        }
        element = list.get(list.size() - 1);
        removeLast();
        return element;
    }

    @Override
    public E getFirst() {
        return list.get(0);
    }

    @Override
    public E getLast() {
        return list.get(list.size() - 1);
    }

    @Override
    public E peekFirst() {
        if (list.get(0) == null) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public E peekLast() {
        if (list.get(list.size() - 1) == null) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (!list.contains(o)) {
            return false;
        }
        list.remove(o);
        return true;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        if (!list.contains(o)) {
            return false;
        }
        int index = list.lastIndexOf(o);
        list.remove(index);
        return false;
    }

    @Override
    public boolean add(E e) {
        list.add(e);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        list.remove(o);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        list.containsAll(c);
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        list.addAll(c);
        return true;
    }

    @Override
    public void push(E e) {
        addFirst(e);
    }

    @Override
    public E pop() {
        E element = list.get(0);
        removeFirst();
        return element;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        list.removeAll(c);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public boolean offer(E e) {
        return list.add(e);
    }

    @Override
    public E remove() {
        return list.remove(0);
    }

    @Override
    public E poll() {
        E element;
        if (list.isEmpty()) {
            return null;
        }
        element = list.get(0);
        list.remove(0);
        return element;
    }

    @Override
    public E element() {
        return list.get(0);
    }

    @Override
    public E peek() {
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public String toString() {
        return "list= " + list;
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }
}
